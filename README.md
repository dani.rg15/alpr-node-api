# Node car-plate recognition service
Node.js app to recognize vehicle lincence plates using "openalpr" service.

### Open ALPR
https://github.com/openalpr/openalpr

### Use

```
npm install 
npm start
```
